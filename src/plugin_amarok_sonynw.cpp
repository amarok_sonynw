/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#define DEBUG_PREFIX "SonyNWMediaDevice"

#include "plugin_amarok_sonynw.h"
#include "trackmediaitem.h"

#include "rubycpp.h"

#include <debug.h>

#include <qtextcodec.h>
#include <qmutex.h>

// we are not reentrant
static QMutex mutex;

AMAROK_EXPORT_PLUGIN( SonyNWMediaDevice )

using namespace Ruby;
using namespace SonyNW;

template <typename T>
static Value v(const T &val) { return Value(val); }

template <>
static Value v(const QString &str) { return Value(str.utf8()); }

SonyNWMediaDevice::SonyNWMediaDevice() : MediaDevice(), m_db( 0 )
{
    QMutexLocker lock(&mutex);
    initialize();
    require("db");
    require("audiofile");
    require("mp3");
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
}

SonyNWMediaDevice::~SonyNWMediaDevice()
{
}

bool SonyNWMediaDevice::isConnected()
{
    return m_db != 0;
}

QStringList SonyNWMediaDevice::supportedFiletypes()
{
    return QStringList("mp3");
}

MediaItem *SonyNWMediaDevice::trackExists( const MetaBundle& bundle )
{
    // FIXME really search
    return 0;
}

bool SonyNWMediaDevice::lockDevice( bool /*tryOnly*/ )
{
    // unsure if that is really possible
    return true;
}

void SonyNWMediaDevice::unlockDevice()
{
}

bool SonyNWMediaDevice::openDevice( bool silent )
{
    QMutexLocker lock(&mutex);
    m_db = new Ruby::Value(klass("NetworkWalkman::Db").send("new", v(mountPoint())));
    if (m_db->isNil()) return false;
    
    Ruby::Value songs = m_db->send("songs");
    for (int i = 0; i < RARRAY(VALUE(songs))->len; i++) {
        Ruby::Value song = rb_ary_entry(songs, i);
        if (VALUE(m_db->const_send("really_exists", song)) == Qfalse)
            continue;
        TrackMediaItem * item = new TrackMediaItem(m_view);
        item->setSong(new Value(song));
    }
    return true;
}

bool SonyNWMediaDevice::closeDevice()
{
    QMutexLocker lock(&mutex);
    delete m_db;
    m_db = 0;
    m_view->clear();
    return true;
}

void SonyNWMediaDevice::synchronizeDevice()
{
    QMutexLocker lock(&mutex);
    m_db->send("save");
}

MediaItem *SonyNWMediaDevice::copyTrackToDevice( const MetaBundle& bundle )
{
    QMutexLocker lock(&mutex);
    // FIXME check if it's a file url
    Value *mp3 = new Value(klass("NetworkWalkman::AudioFile::Mp3").send("new", v(bundle.url().path())));
    mp3->send("putOnDevice", v(mountPoint()));
    m_db->send("add", *mp3);
    TrackMediaItem * item = new TrackMediaItem(m_view);
    item->setSong(mp3);
    return item;
}

int SonyNWMediaDevice::deleteItemFromDevice( MediaItem *it, int flags )
{
    QMutexLocker lock(&mutex);
    TrackMediaItem * item = dynamic_cast<TrackMediaItem *>(it);
    if (item == 0)
        return 0;
    Value song = *(item->song());
    m_db->send("remove", song);
    delete it;
    return 1;
}

#include "plugin_amarok_sonynw.moc"
