/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "trackmediaitem.h"

#include "rubycpp.h"

#include <debug.h>

using namespace Ruby;

namespace SonyNW {

TrackMediaItem::~TrackMediaItem()
{
    delete m_song;
}

static inline const char * getTag(const Value &song, const char * tagname)
{
    Value tags = song.const_send("tags");
    if (tags.isNil())
        return 0;

    Value tag = tags.const_send("[]", tagname);
    if (tag.isNil())
        return 0;

    tag.send("recode", Fixnum(3));
    Value text = tag.const_send("[]", "text");
    if (text.isNil())
        return 0;

    return text;
}

void TrackMediaItem::setSong(Ruby::Value* theValue)
{
    m_song = theValue;
    m_order = theValue->const_send("slot").to_i();
    setText(0, getTag(*theValue, "TITLE"));
}

}
