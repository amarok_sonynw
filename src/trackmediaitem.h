/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SONYNWTRACKMEDIAITEM_H
#define SONYNWTRACKMEDIAITEM_H

#include <mediabrowser.h>

namespace Ruby {
    class Value;
}

namespace SonyNW {

/**
	@author Rafał Rzepecki <divided.mind@gmail.com>
*/
class TrackMediaItem : public MediaItem
{
public:
    TrackMediaItem(QListView * view) : MediaItem(view), m_song( 0 ) {}
    ~TrackMediaItem();

    void setSong(Ruby::Value* theValue);
    

    Ruby::Value* song() const
    {
        return m_song;
    }
    
protected:
    Ruby::Value * m_song;
};

}

#endif
