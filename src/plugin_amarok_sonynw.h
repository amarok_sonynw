/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _PLUGIN_AMAROK_SONYNW_H_
#define _PLUGIN_AMAROK_SONYNW_H_

#include "mediabrowser.h"

#include <qmap.h>

namespace Ruby {
    class Value;
};

class SonyNWMediaDevice : public MediaDevice
{
Q_OBJECT
public:
    SonyNWMediaDevice();
    virtual ~SonyNWMediaDevice();
    virtual bool isConnected();
    virtual QStringList supportedFiletypes();

private:
    virtual MediaItem *trackExists( const MetaBundle& bundle );

protected:
    virtual bool lockDevice( bool tryOnly = false );
    virtual void unlockDevice();
    virtual bool openDevice( bool silent = false );
    virtual bool closeDevice();
    virtual void synchronizeDevice();
    virtual MediaItem *copyTrackToDevice( const MetaBundle& bundle );
    virtual int deleteItemFromDevice( MediaItem *item, int flags = DeleteTrack );
protected:
    Ruby::Value *m_db;
};

#endif // _PLUGIN_AMAROK_SONYNW_H_
