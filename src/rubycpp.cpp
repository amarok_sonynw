/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "rubycpp.h"
#include <iostream>
#include <sstream>

using namespace std;

namespace Ruby
{
    void dump_trace() {
        cerr << "Ruby exception" << endl;
        // position
        std::ostringstream where;
        where << ruby_sourcefile << ":" << ruby_sourceline;
        ID id = rb_frame_last_func();
        if(id) {
                where << ":in `" << rb_id2name(id) << "'";
        }
        cerr << "where: " << where.str() << endl;

        VALUE exception_instance = rb_gv_get("$!");

        // class
        VALUE klass = rb_class_path(CLASS_OF(exception_instance));
        cerr << "class: " << RSTRING(klass)->ptr << endl;

        // message
        VALUE message = rb_obj_as_string(exception_instance);
        cerr << "message: " << RSTRING(message)->ptr << endl;

        // backtrace
        if(!NIL_P(ruby_errinfo)) {
                std::ostringstream o;
                VALUE ary = rb_funcall(ruby_errinfo, rb_intern("backtrace"), 0);
                int c;
                for (c=0; c<RARRAY(ary)->len; c++) {
                        o << "\tfrom " << RSTRING(RARRAY(ary)->ptr[c])->ptr << "\n";
                }
                cerr << "backtrace: " << o.str() << endl;
        }
    }

    static VALUE doRequire(VALUE module)
    {
        rb_require(reinterpret_cast<const char *>(module));
        return Qnil;
    }

    void require(const char * module)
    {
//        cerr << "requiring " << module << endl;
        int error = 0;
        rb_protect(doRequire, reinterpret_cast<VALUE>(module), &error);
        if (error)
            dump_trace();
    }
    
    Value::Value(VALUE value) : self(value)
    {
        rb_gc_register_address(&self);
    }

    Value::~Value()
    {
        rb_gc_unregister_address(&self);
    }

    static VALUE GetClass(VALUE name)
    {
        return rb_path2class(reinterpret_cast<const char *>(name));
    }

    Value klass(const char * name)
    {
//         cout << "getting class " << name << endl;
        int error = 0;
        VALUE ret = rb_protect(GetClass, reinterpret_cast<VALUE>(name), &error);
        if (error)
            dump_trace();
        return ret;
    }

    struct Args
    {
        VALUE recv;
        ID id;
        int n;
        VALUE *argv;
    };

    static VALUE Send(VALUE a)
    {
        struct Args *args = reinterpret_cast<struct Args *>(a);
//         cerr << args << endl;
//         cerr << "sending " << rb_id2name(args->id) << " with " << args->n << " arguments" << endl;
        return rb_funcall2(args->recv, args->id, args->n, args->argv);
    }

    Value Value::send(const char * method)
    {
        struct Args a = {self, rb_intern(method), 0, 0};
//        cout << "sending " << method << endl;
        int error = 0;
        VALUE ret = rb_protect(Send, reinterpret_cast<VALUE>(&a), &error);
//        cerr << "sent" << endl;
        if (error)
            dump_trace();
        return ret;
    }

    Value Value::send(const char * method, Value arg)
    {
        VALUE _arg = arg;
        struct Args a = {self, rb_intern(method), 1, &_arg};
//         cout << "sending " << method << endl;
        int error = 0;
        VALUE ret = rb_protect(Send, reinterpret_cast<VALUE>(&a), &error);
        if (error)
            dump_trace();
        return ret;
    }


    void initialize()
    {
        static bool initialized = false;
        if (initialized) return;
        ruby_init();
        ruby_init_loadpath();
//         cout << "initializing" << endl;
        initialized = true;
    }

};
