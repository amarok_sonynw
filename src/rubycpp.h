/***************************************************************************
 *   Copyright (C) 2008 by Rafał Rzepecki   *
 *   divided.mind@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _RUBY_CPP_H_
#define _RUBY_CPP_H_

#include <ruby.h>
#include <string>
#include <iostream>

using namespace std;

namespace Ruby
{
    void require(const char * module);
    void initialize();

class Value
{
    public:
        Value() : self(Qnil) {}
        Value(VALUE value);
        ~Value();
        bool isNil() const { return NIL_P(self); }
        Value send(const char * method);
        Value const_send(const char * method) const { return const_cast<Value *>(this)->send(method); }
        Value send(const char * method, Value arg);
        Value const_send(const char * method, Value arg) const { return const_cast<Value *>(this)->send(method, arg); }
        operator VALUE() { return self; }
        operator const VALUE() const { return self; }
        const char * inspect() const { return STR2CSTR(const_send("inspect")); }

        /* implicit char * conversion */
        Value(const char * string) : self(rb_str_new2(string)) {}
        operator const char *() const { return STR2CSTR(self); }

        int to_i() const { return FIX2INT(self); }
    protected:
        VALUE self;
    private:
        operator void *() { return 0; /* no implicit conversion */ }
        virtual operator int() { return 0; /* no implicit conversion */ }
};

    Value klass(const char * name);

    class Fixnum : public Value
{
    public:
        Fixnum(int number) : Value(INT2FIX(number)) {}
};
}

#endif
