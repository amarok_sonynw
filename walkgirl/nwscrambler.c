
#include <ruby.h>

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

// FIXME assumes 64-bit long long and big endian
static VALUE t_scramble(VALUE self, VALUE outfile)
{
  VALUE file;
  unsigned long long key;
  unsigned long audioStartX, audioEndX, left;
  int in_fd, out_fd;
  char buf[8];

  file = rb_iv_get(self, "@file");
  
  audioStartX = FIX2ULONG(rb_iv_get(self, "@audioStartX"));
  audioEndX = FIX2ULONG(rb_iv_get(self, "@audioEndX"));
  key = 0xFFFFFFFF & (( 0x2465 + FIX2UINT(rb_iv_get(self, "@slot")) * 0x5296E435 ) ^ FIX2UINT(rb_iv_get(self, "@dvid")));
  // XXX assumes big endian
  key = ((key & 0xFF      ) << 24) |
        ((key & 0xFF <<  8) <<  8) |
        ((key & 0xFF << 16) >>  8) |
        ((key & 0xFF << 24) >> 24);
  key = (key << 32) | key;

  rb_funcall(file, rb_intern("sync="), 1, Qtrue);
  in_fd = FIX2INT(rb_funcall(file, rb_intern("fileno"), 0));

  rb_funcall(outfile, rb_intern("sync="), 1, Qtrue);
  out_fd = FIX2INT(rb_funcall(outfile, rb_intern("fileno"), 0));

  lseek(in_fd, audioStartX, SEEK_SET);
  left = audioEndX - audioStartX;
  while (left > 7 && read(in_fd, buf, 8)) {
    *((unsigned long long *) buf) ^= key;
    write(out_fd, buf, 8);
    left -= 8;
  }

  if (left) {
    read(in_fd, buf, left);
    write(out_fd, buf, left);
  }
  return Qtrue;
}

/*
Equivalent in ruby:

def scramble(outfile)
        key = 0xFFFFFFFF & (( 0x2465 + @slot * 0x5296E435 ) ^ @dvid);
        @file.seek(@audioStartX)
        left = @audioEndX - @audioStartX
        while left > 0 and block = @file.read([8, left].min)
          if block.length != 8
            outfile << block
          else
            outfile << block.unpack("N2").map{|x|x^key}.pack("N2")
          end
          left -= block.length
        end
      end
*/

VALUE Mp3;

void Init_nwscrambler()
{
  Mp3 = rb_gv_get("NetworkWalkman::Db::Mp3");
  rb_define_method(Mp3, "scramble", t_scramble, 1);
}
