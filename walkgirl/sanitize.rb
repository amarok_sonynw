############################################################################
#    Copyright (C) 2008 by Rafał Rzepecki   #
#    divided.mind@gmail.com   #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

require 'walkgirl'

mount_point = ARGV[0]

db = NetworkWalkman::Db.new(mount_point, false)
db.songs = []
db.collections.each { |_, c| c.clear }

afs = NetworkWalkman::AudioFile.openAll(mount_point)
# afs.each { |af|
#   if af
#     db.songs[af.slot-1] = af
#     db.collections.each { |k, c|
#       key = af.tags[k]["text"] if af.tags[k] && af.tags[k]["text"]
#       key ||= "Other"
#       c[key] << af.slot
#     }
#   end
# }
# 
# p db.songs.map{|s|
#   if s.nil?
#     "nil"
#   elsif s.tags && s.tags["TITLE"] && s.tags["TITLE"]["text"]
#     s.tags["TITLE"]["text"]
#   else
#     s.slot.to_s
#   end
# }
# p db.collections

afs.each { |af|
  db.add af if af
}

db.save
