#!/usr/bin/ruby
# == Synopsis
#
# sonynwput: scrambles an MPEG audio file for Sony Network Walkman
#
# == Usage
#
# sonynwput [OPTION] ... MPGFILE
#
# -h, --help:
#    show help
#
# -k 0xDEADBEEF, --dvid 0xDEADBEEF:
#    use this device id key to scramble
#
# -d DEVICEPATH, --device DEVICEPATH
#    put file on the device mounted at DEVICEPATH
#
# -s N, --slot N
#    scrambles for slot number N
#
# -o FILE, --out FILE
#    outputs to FILE instead of stdout
#
# MPGFILE: the file to scramble.
#
# It is mandatory to give slot. If DEVICEPATH is not given
# or there is no mp3fm/dvid.dat file on the device, a dvid
# needs also be given.
#
# == Comments
#
# File scrambled for slot number N has to be put in
# printf("OMGAUDIO\\10F%02x\\1%07x.OMA", N >> 8, N)
# on the device in order to be descrambled correctly.
#
# Currently the tool does its best to also copy tags in
# the way the device could read.
# However, please note that not all encodings are
# covered by the device charset.
#
# Tested with NW-507.
#
# == Bugs
#
# - VBR file support hasn't been tested yet and possibly is flaky or doesn't work at all.
# - Broken mpeg files produce OMA files which don't play at all.
#
# == Author
#
# Please forward flowers and flames to Rafał Rzepecki <divided.mind@gmail.com>.

require 'Walkman'
require 'getoptlong'
require 'rdoc/usage'

opts = GetoptLong.new(
  [ "--slot",   "-s", GetoptLong::REQUIRED_ARGUMENT ],
  [ "--dvid",   "-k", GetoptLong::REQUIRED_ARGUMENT ],
  [ "--device", "-d", GetoptLong::REQUIRED_ARGUMENT ],
  [ "--out",    "-o", GetoptLong::REQUIRED_ARGUMENT ],
  [ "--help",   "-h", GetoptLong::NO_ARGUMENT ]
)

slot = nil
dvid = nil
device = nil
outfile = STDOUT

opts.each do |opt, arg|
  case opt
  when '--help'
    RDoc::usage
  when '--slot'
    slot = arg.to_i
  when '--dvid'
    dvid = arg.hex
  when '--device'
    device = arg
  when '--out'
    outfile = File.new(arg, 'w+')
  end
end

mp3file = ARGV[0]

if device and not dvid
  f = File.new(File.join([device, "mp3fm", "dvid.dat"]))
  f.seek(10)
  dvid = f.read(4).unpack("N")[0]
end

if device
  dir = sprintf "10f%02x", slot >> 8
  fname = sprintf "1%07x.oma", slot
  outfile = File.new(File.join([device, "omgaudio", dir, fname]), 'w+')
end

if !slot || !dvid || !outfile || !mp3file
  RDoc::usage("Usage")
end

mp3 = Walkman::Oma.new(slot, dvid).fromMp3(mp3file)
puts "Scrambling... "
mp3.write(outfile)
