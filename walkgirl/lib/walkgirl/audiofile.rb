############################################################################
#    Copyright (C) 2008 by Rafał Rzepecki   #
#    divided.mind@gmail.com   #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

require 'walkgirl/id3'

module NetworkWalkman
  class AudioFile
    MPEG_VERSION = [:v2_5, :reserved, :v2, :v1]
    attr_accessor :length, :frames, :tags, :is_vbr
    attr_reader :filename, :slot
    class FromDevice < AudioFile
      attr_reader :encoding
      def initialize(*args)
        @length, @frames, @tags, @filename, @slot, @encoding = args
      end
      def scramble(outfile)
        # noop
      end
    end

    def self.open(filename)
      File.open(filename) { |f|
        begin
          slot = filename.match("1([0-9a-f]{7})\.")[1].hex
          tag_data = f.read(3072)
          f.seek(0xc22)
          encoding, _, length, frames = f.read(10).unpack("CCNN")
          f.close
          tag_data[0..4] = ["ID3", 4, 0].pack("a3CC")
          tags = ID3::Tag2.new
          tags.raw = tag_data
          tags.parse!
          tags.each { |key, tag|
            begin
              tags[key] = tag.recode(0)
            rescue
              begin # try harder to recode
                tag = tag.clone
                tag["text"] = tag["text"][0...(tag["text"].length-1)]
                tags[key] = tag.recode(0)
              rescue
              end
            end
          }
          return FromDevice.new(length, frames, tags, filename, slot, encoding)
        rescue
          STDERR.puts("Corrupted file #{filename}")
        end
      }
    end

    def self.openAll(mount_point)
      files = []
      Dir[File.join([mount_point, 'omgaudio', '10f*'])].sort.each { |dir|
        Dir[File.join([dir, "*.oma"])].sort.each do |filename|
          af = AudioFile.open(filename)
          files[af.slot-1] = af if af
        end
      }
      files
    end

    def self.filename_for_slot(slot, mount_point = '.')
      File.join([mount_point, 'omgaudio', sprintf('10f%02x', slot >> 8), sprintf('1%07x.oma', slot & 0xff)])
    end

    def self.freeSlot(mount_point)
      slot = 1
      while (File.exists?(filename_for_slot(slot, mount_point))) do slot += 1 end
      slot
    end

    def encoding
      (mpeg_version << 6) |
      (mpeg_layer   << 4) |
      (bitrate_id       )
    end

    IMPORTANT_TAGS = [ 'ARTIST', 'TITLE', 'ALBUM', 'CONTENTTYPE' ]
    def save
      begin
        Dir.mkdir(File.dirname filename)
      rescue
      end
      File.open(filename, 'w+') {|outfile|
        tag = ID3::Tag2.new
        if tags # convert to ID3v2
          # important tags go first
          IMPORTANT_TAGS.each { |key|
            if tags[key]
              value = tags[key]
              tag[key] = value
              tag[key] = tag[key].recode(2) if tag[key]["text"]
            end
          }
          important_tags = tag.dump[0...-32]
          tag = ID3::Tag2.new
          (tags.keys - IMPORTANT_TAGS).each { |key|
            value = tags[key]
            tag[key] = value
            tag[key] = tag[key].recode(2) if tag[key]["text"]
          }
        end
        tag = important_tags + tag.dump[10..-1]
        tag[0...10] = ["ea3", 3, 0x1776].pack("a3c@8n")
        outfile.sysseek(0, IO::SEEK_SET)
        outfile.syswrite [tag].pack("a3072")
        # write out header
        outfile.syswrite "EA3"
        outfile.syswrite [2, 0, 0x60, 0xff, 0xfe, 1, 0x0f, 0x50, 0x00].pack("c5@9c4")
        outfile.syswrite [0, 4, 0, 0, 0, 1, 2, 3, 0xc8, 0xd8, 0x36, 0xd8, 0x11, 0x22, 0x33, 0x44].pack("c*")
        outfile.syswrite [0x03, if is_vbr then 0x90 else 0x80 end, encoding, 10, length, frames, 0].pack("c4N3")
        outfile.syswrite [0,0,0,0].pack("N4")
        outfile.syswrite [0,0,0,0].pack("N4")
        outfile.syswrite [0,0,0,0].pack("N4")
        outfile.sysseek outfile.tell
        scramble(outfile)
      }
    end

    def initialize
    end
    def track_length
      @length
    end
    def tags
      @tags
    end
    def frame_count
      @frames
    end
  end
end
