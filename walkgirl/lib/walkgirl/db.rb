############################################################################
#    Copyright (C) 2008 by Rafał Rzepecki   #
#    divided.mind@gmail.com   #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

require 'iconv'
require 'walkgirl/id3'

class OrderedHash < Hash
  alias_method :retrieve, :[]
  alias_method :store, :[]=
  alias_method :each_pair, :each

  def initialize
    @keys = []
  end

  def keys
    @keys
  end

  def [](key)
    if key.is_a? Integer
      retrieve @keys[key]
    else
      super
    end
  end

  def []=(key, val)
    if key.is_a? Integer
      store @keys[key], val
    else
      @keys << key unless include? key
      super
    end
  end

  def delete(key)
    @keys.delete(key)
    super
  end

  def each
    @keys.each { |k| yield k, self[k] }
  end

  def each_key
    @keys.each { |k| yield k }
  end

  def each_value
    @keys.each { |k| yield self[k] }
  end

  def clear
    @keys.clear
    super
  end
end

module ID3
  TAG_LEN = 128
  class Frame
    def dump_short(key = "TIT2")
      tag = self
      tag = self.recode(2)
      begin
        [key, 0, tag.data].pack("a4ca#{TAG_LEN-5}")
      rescue
        [key, 0, ""].pack("a4ca#{TAG_LEN-5}")
      end
    end
  end
end

module NetworkWalkman
  module Song
    attr_accessor :slot
    def path
      File.join(['omgaudio', sprintf('10f%02x', slot >> 8), sprintf('1%07x.oma', slot & 0xff)])
    end
  end
  class Db
    COLLECTION_IDS = { "CONTENTGROUP" => 1, "ARTIST" => 2, "ALBUM" => 3, "CONTENTTYPE" => 4}
    CNTFILE = "04cntinf.dat"
    attr_accessor :songs
    attr_accessor :collections
    attr_accessor :mount_point
    
    def initialize(mount_point, parse = true)
      @mount_point = mount_point

      if parse
        cnt = dbfile(CNTFILE)
        slot = 1
        @songs = cnt.objects[0].records.map { |cnfb|
          song = cnfb
          song.tags.each { |key, tag|
            song.tags[key] = tag.recode(3)
          }
          song.extend Song
          song.slot = slot
          slot += 1
          song
        }

        @collections = {}
        COLLECTION_IDS.keys.each { |collection|
          collection_id = COLLECTION_IDS[collection]
          @collections[collection] = Collection.new(collection_id, self)
        }
      else
        @songs = []
        @collections = {}
        COLLECTION_IDS.keys.each { |collection|
          collection_id = COLLECTION_IDS[collection]
          @collections[collection] = Collection.new(collection_id, self, false)
        }
      end
    end

    def remove(song)
      # first remove it from collections
      collections.each { |_, collection|
        collection.each { |key, songs|
          songs.delete song
          collection.delete key if songs.empty?
        }
      }

      # then blank all data fields
      song.tags = ID3::Tag2.new
      song.track_length = 0
      song.frame_count = 0

      # finally delete the file
      File.delete(File.join([mount_point, song.path]))
    end

    def really_exists(song)
      File.exists?(File.join([mount_point, song.path]))
    end

    def add(song)
      songs[song.slot-1] = song
      collections.each { |k, c|
        key = song.tags[k].recode(3)["text"] if song.tags[k] && song.tags[k]["text"]
        key ||= "Other"
        c[key] << song
      }
    end

    IMPORTANT_TAGS = [ 'ARTIST', 'TITLE', 'ALBUM', 'CONTENTTYPE' ]
    def save
      gtr = DbFile.new
      gtr.magic = 'GTLT'
      sysb = DbObject.new
      sysb.magic = 'SYSB'
      sysb.rec_size = 80
      record = Record::Record.new
      sysb.records << record
      gtr.objects['SYSB'] = sysb
      gtlb = DbObject.new
      gtlb.magic = 'GTLB'
      gtlb.rec_size = 80

      record = Record::Gtlb.new
      record.record_number = 1
      record.record_group = 1
      gtlb.records << record
      record = Record::Gtlb.new
      record.record_number = 2
      record.record_group = 3
      record.tag = 'TPE1'
      gtlb.records << record
      record = Record::Gtlb.new
      record.record_number = 3
      record.record_group = 3
      record.tag = 'TALB'
      gtlb.records << record
      record = Record::Gtlb.new
      record.record_number = 4
      record.record_group = 3
      record.tag = 'TCON'
      gtlb.records << record
      record = Record::Gtlb.new
      record.record_number = 22
      record.record_group = 2
      record.tag = nil
      gtlb.records << record
      (5...34).each do |i|
        record = Record::Gtlb.new
        record.record_group = 0
        record.record_number = i
        gtlb.records << record
      end
      gtr.objects['GTLB'] = gtlb
      File.open(dbfilename('00gtrlst.dat'), 'w') do |file|
        file << gtr
      end

      gtif = DbFile.new
      gtif.magic = 'GTIF'
      gtfb = DbObject.new
      gtfb.magic = 'GTFB'
      (0...64).each do
        record = Record::Cnfb.new
        record.track_type = [0, 0, 0, 0]
        record.track_length = 0
        record.tags = ID3::Tag2.new
        record.frame_count = 0
        record.tags['TITLE'] = ''
        gtfb.records << record
      end
      gtif.objects['GTFB'] = gtfb
      File.open(dbfilename('02treinf.dat'), 'w') do |f|
        f << gtif
      end

      cidl = DbFile.new
      cidl.magic = 'CIDL'
      cilb = DbObject.new
      cilb.magic = 'CILB'
      cilb.rec_size = 48
      @songs.each do
        record = ([1, 0xf, 0x50, 4, 1, 2, 3, 0xc8, 0xd8, 0x36, 0xd8].pack("@4C3@9C@13C3@16C4@48"))
        cilb.records << record
      end
      cidl.objects['CILB'] = cilb
      File.open(dbfilename('05cidlst.dat'), 'w') do |f|
        f << cidl
      end
      
      cnt = DbFile.new
      cnt.magic = 'CNIF'
      songs = DbObject.new
      songs.magic = 'CNFB'
      songs.rec_size = 656
      @songs.each { |song|
        record = Record::Cnfb.new
        record.track_type = Record::Cnfb::OBFUSCATED_MP3
        record.track_length = 0
        record.tags = ID3::Tag2.new
        record.frame_count = 0
        if song
          tag = ID3::Tag2.new

          # choose right tags up to 5 tags limit
          left = 5
          song.tags.each do |k, v|
            if IMPORTANT_TAGS.include? k
              tag[k] = v
              left -= 1
            end
          end
          song.tags.each do |k, v|
            if left > 0 and !IMPORTANT_TAGS.include? k
              tag[k] = v
              left -= 1
            end
          end

          record.track_length = song.track_length
          record.tags = tag
          record.frame_count = song.frame_count
        end
        songs.records << record
      }
      cnt.objects['CNFB'] = songs
      cntfile = File.new(dbfilename(CNTFILE), 'w')
      cntfile << cnt
      cntfile.close
      @collections.values.each { |collection|
        collection.save
      }
      nil
    end

    class Collection < OrderedHash
      def initialize(id, db, load = true)
        super()
        @db = db
        @id = id
        @tree_filename = sprintf("01tree%02x.dat", @id)
        @ginf_filename = sprintf("03ginf%02x.dat", @id)

        if load
          tree = @db.dbfile(@tree_filename)
          gplb = tree.objects['GPLB']
          tplb = tree.objects['TPLB']
          ginf = @db.dbfile(@ginf_filename)
          gpfb = ginf.objects['GPFB']

          gpfb.records.each { |r| self[r.text] = [] }
          
          starts = OrderedHash.new
          gplb.records.sort_by{|r|r.starts_on_list}.each{ |r|             starts[self.keys[r.description_slot-1]] = r.starts_on_list
          }
          starts
          (0...starts.length).each{ |i|
            last = starts[i+1] || (tplb.records.length + 1)
            self[starts.keys[i]] = tplb.records.map{|r|r.song_slot}[(starts[i]-1)...(last-1)].map{|slot| @db.songs[slot-1] }
          }
        end
      end

      def [](key)
        super || (self[key] = [])
      end

      def save
        ginf = DbFile.new
        ginf.magic = 'GPIF'
        gpfb = DbObject.new
        gpfb.magic = 'GPFB'

        tree = DbFile.new
        tree.magic = 'TREE'
        gplb = DbObject.new
        gplb.magic = 'GPLB'
        gplb.obj_size = 0x4010
        tplb = DbObject.new
        tplb.magic = 'TPLB'

        didx = 1
        sidx = 1
        self.keys.each {|key|
          record = Record::Gpfb.new
          record.text = key
          gpfb.records << record

          record = Record::Gplb.new
          record.description_slot = didx
          didx += 1
          record.starts_on_list = sidx
          sidx += self[key].length
          gplb.records << record
          self[key].each{ |s|
            record = Record::Tplb.new
            record.song_slot = s.slot
            tplb.records << record
          }
        }
        ginf.objects['GPFB'] = gpfb
        tree.objects['GPLB'] = gplb
        tree.objects['TPLB'] = tplb
        ginffile = File.new(@db.dbfilename(@ginf_filename), 'w')
        ginffile << ginf
        ginffile.close
        treefile = File.new(@db.dbfilename(@tree_filename), 'w')
        treefile << tree
        treefile.close
      end
    end

    def dbfilename(name)
      File.join([@mount_point, "omgaudio", name])
    end
    def dbfile(name)
      DbFile.new(File.open(dbfilename(name)){|file|file.read})
    end

    module Id3
      FRAME2SYMBOL = ID3::Framename2symbol["2.4.0"]
      SYMBOL2FRAME = ID3::SUPPORTED_SYMBOLS["2.4.0"]

      class Frame < Hash
        def initialize(*a)
          if a[0].is_a?(Hash)
            self.merge! a[0]
          end
        end
        def to_s(*a)
          limit = ""
          if a[0]
            limit = a[0] - 6
          end
          [SYMBOL2FRAME[self[:tag]], self[:encoding], Id3.encode(self[:encoding], self[:content])].
          pack "a4nZ#{limit}"
        end
      end
      
      def Id3.frame(tag, content, encoding)
        tag = FRAME2SYMBOL[tag] || tag
        content = content
        Frame.new :tag => tag, :content => Id3.decode(encoding, content), :encoding => encoding
      end
      def Id3.decode(encoding, content)
        case encoding
        when 2: # UTF-16BE
          Iconv.conv('utf8', 'utf16be', content)
        else
          raise "Unknown id3 encoding (#{encoding})"
        end
      end
      def Id3.encode(encoding, content)
        case encoding
        when 2: # UTF-16BE
          Iconv.conv('utf16be', 'utf8', content)
        else
          raise "Unknown id3 encoding (#{encoding})"
        end
      end
    end
    
    module Record
      class Record
        def initialize(*a)
          if a.length == 1 and a[0].is_a?(String)
            parse a[0]
          end
        end
      end

      class Tplb < Record
        attr_accessor :song_slot
        def parse(content)
          @song_slot = content.unpack("n")[0]
        end
        def to_s
          [@song_slot].pack("n")
        end
        def length
          2
        end
      end

      class Gpfb < Record
        attr_accessor :text
        def to_s
          [0, 0, 0x2eeeb, 1, 128].pack("N3n2") + ID3::Frame.new("ARTIST", "2.4.0", "\3#{@text}").recode(2).dump_short
        end
        def parse(content)
          frame = ID3::Frame.new("TITLE", "2.4.0", content.unpack("a21A*")[1])
          @text = frame.recode(3)["text"]
        end
        def length
          16 + 128
        end
      end

      class Gplb < Record
        attr_accessor :description_slot
        attr_accessor :starts_on_list # index of where it starts in Tplb
        def parse(content)
          @description_slot, _, @starts_on_list, _ = content.unpack("n4")
        end
        def to_s
          [@description_slot, 0x0100, @starts_on_list, 0x0000].pack("n4")
        end
        def length
          8
        end
      end

      class Cnfb < Record
        attr_accessor :track_length, :frame_count, :tags, :content
        attr_reader :track_type
        OBFUSCATED_MP3 = [ 0, 0, 0xFF, 0xFE ]
        PLAIN_MP3 = [ 0, 0, 0xFF, 0xFF ]
        TAG_LEN = 128
        def track_type=(new_type)
          @track_type = new_type
          @track_type.instance_eval do
            def inspect
              case self
              when OBFUSCATED_MP3:
                "OBFUSCATED_MP3"
              when PLAIN_MP3:
                "PLAIN_MP3"
              else
                super
              end
            end
            def to_s
              pack("C4")
            end
          end
        end
        def parse(contents)
          track_type, @track_length, @frame_count, num_tags, tag_size, contents =
          contents.unpack("a4NNnna*")

          self.track_type = track_type.unpack("C4")

          @tags = ID3::Tag2.new
          (0...num_tags).each do
            tag, contents = contents.unpack("a#{tag_size}a*")
            tag, _, content = tag.unpack("a4aA*")
            symbol = ID3::Framename2symbol["2.4.0"][tag]
            if symbol
              @tags[symbol] = ID3::Frame.new(symbol, "2.4.0", content)
            end
          end
        end
        def to_s
          s = [@track_type.to_s, @track_length, @frame_count, @tags.length, TAG_LEN].pack("a4NNnn@16") +
          @tags.map do |t,v|
            v.dump_short(ID3::SUPPORTED_SYMBOLS["2.4.0"][t])
          end.join
        end
        def length
          16 + @tags.length * TAG_LEN
        end
      end

      class Gtlb < Record
        attr_accessor :record_number, :record_group, :tag
        def initialize
          @tag = nil
        end
        def to_s
          [@record_number, @record_group, tag.nil?? 0 : 1, 0, @tag || ""].pack("nn@16nna4")
        end
        def length
          80
        end
      end
    end

    class DbObject
      attr_accessor :magic, :records, :rec_size, :obj_size
      def initialize(*a)
        @records = []
        @magic = "CAFE"
        if a.length == 2 and a[1].is_a?(String)
          parse a[1]
        end
      end
      def parse(contents)
        @magic, rec_count, @rec_size, contents = contents.unpack("a4nnx8a*")
        record_klass = begin
          Record.const_get(magic.capitalize)
        rescue NameError
          String
        end
        (0...rec_count).each do
          record, contents = contents.unpack("a#{rec_size}a*")
          @records << record_klass.new(record)
        end
      end
      def to_s
        @rec_size ||= @records[0].length if @records[0]
        @rec_size ||= 0
        res = [@magic, @records.length, rec_size, @records.length].pack("a4nnN@16") +
          @records.map{|r|[r.to_s].pack("Z#{rec_size}")}.join
        if obj_size
          [res].pack("a#{obj_size}")
        else
          res
        end
      end
    end

    class DbObjectPointer
      attr_reader :magic, :range
      def initialize(*a)
        if a.length == 1 and a[0].is_a?(String)
          parse a[0]
        elsif a.length == 3
          @magic, @range = a[0], (a[1]...(a[1]+a[2]))
        end
      end
      def parse(contents)
        @magic, offset, length = contents.unpack("a4NN")
        @range = offset...(offset+length)
      end
      def to_s
        [magic, range.first, range.last - range.first].pack("a4NN@16")
      end
    end

    class DbFile
      attr_accessor :magic, :objects
      def initialize(*a)
        @objects = OrderedHash.new
        if a.length == 1 and a[0].is_a?(String)
          parse a[0]
        end
      end
      def parse(contents)
        @magic, numobjects = contents.unpack("a4x4C")
        objects = []
        (1..numobjects).each do |objindex|
          start = objindex * 16
          fin = objindex * 16 + 16
          objects << DbObjectPointer.new(contents[start...fin])
        end
        objects.each do |obj_pointer|
          @objects[obj_pointer.magic] = DbObject.new(obj_pointer.magic, contents[obj_pointer.range])
        end
      end
      def to_s
        header = [@magic, 1, 1, 0, 0, objects.length].pack("a4C4C@16")
        pointers = ""
        objects = ""
        offset = 16 + @objects.length * 16
        @objects.each_value do |object|
          object_raw = object.to_s
          pointers += DbObjectPointer.new(object.magic, offset, object_raw.length).to_s
          offset += object_raw.length
          objects += object_raw
        end
        header + pointers + objects
      end
    end
  end
end

