############################################################################
#    Copyright (C) 2008 by Rafał Rzepecki   #
#    divided.mind@gmail.com   #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

require 'walkgirl/id3'
require 'walkgirl/audiofile'
require 'nwscrambler'

module NetworkWalkman
  def self.get_dvid(mount_point)
    File.open(File.join([mount_point, "mp3fm", "dvid.dat"])) {|f|
      f.seek(10)
      f.read(4).unpack("N")[0]
    }
  end

  class AudioFile
    class Mp3 < AudioFile
      MPEG_VERSION = [:v2_5, :reserved, :v2, :v1]
      LAYER = [:reserved, :layer3, :layer2, :layer1]
      BITRATE = [
          # v1l1 v1l2 v1l3 v2l1 v2l*
  [        0, 0, 0, 0, 0],
  [        32, 32, 32, 32, 8],
  [        64, 48, 40, 48, 16],
  [        96, 56, 48, 56, 24],
  [        128, 64, 56, 64, 32],
  [        160, 80, 64, 80, 40],
  [        192, 96, 80, 96, 48],
  [        224, 112, 96, 112, 56],
  [        256, 128, 112, 128, 64],
  [        288, 160, 128, 144, 80],
  [        320, 192, 160, 160, 96],
  [        352, 224, 192, 176, 112],
  [        384, 256, 224, 192, 128],
  [        416, 320, 256, 224, 144],
  [        448, 384, 320, 256, 160],
  [        -1, -1, -1, -1, -1]].transpose

      SAMPLING_RATE = { :v1 => [44100, 48000, 32000, -1],
      :v2 => [22050, 24000, 16000, -1],
      :v2_5 => [11025, 12000, 8000, -1] }
      SAMPLE_PER_FRAME = { :v1 => [0, 1152, 1152, 384],
      :v2 => [0, 576, 1152, 384],
      :v2_5 => [0, 576, 1152, 384]}
      def bitrates
        if version == :v1
          case layer
          when :layer1:
            BITRATE[0]
          when :layer2
            BITRATE[1]
          when :layer3
            BITRATE[2]
          end
        elsif layer == :layer1
          BITRATE[3]
        else
          BITRATE[4]
        end
      end
      attr_reader :version, :layer, :tags, :bitrate, :length, :frames, :encoding

      def initialize(filename)
        @tags = ID3::AudioFile.new(filename)
        @filename = File.basename(filename)
        @file = File.new(filename)
        @file.seek(@tags.audioStartX)

        # find first frame
        loop do
          loop do
            if @file.readchar == 0xFF
              @tags.audioStartX = @file.pos
              if (byte = @file.readchar) & 0xE0 == 0xE0
                @version = MPEG_VERSION[(byte & 0x18) >> 3]
                @layer = LAYER[(byte & 0x06) >> 1]
                @has_crc = (byte & 1) == 0
                @encoding = (byte & 0x1e) << 3
                break
              end
            end
          end

          byte = @file.readchar
          @bitrate = bitrates[(byte & 0xF0) >> 4]
          if @bitrate == 0
            next
          end
          @sampling_rate = SAMPLING_RATE[version][(byte & 0x0C) >> 2]
          @is_padded = (byte & 2) == 2
          @length = (tags.audioEndX - tags.audioStartX) * 8 / bitrate
          @sample_per_frame = SAMPLE_PER_FRAME[version][LAYER.index(layer)]
          @frames = @length * @sampling_rate / 1000 / @sample_per_frame
          @framelen = bitrate * 1000 * @sample_per_frame / 8 / @sampling_rate 
          @encoding |= (byte & 0xF0) >> 4
          if @has_crc
            @crc = @file.read(2).unpack("n")
          end
          @is_vbr = @file.read(37).unpack("@33a4")[0].upcase == "XING"
          if @sampling_rate > 0
            break
          end
        end
        @audioStartX, @audioEndX = tags.audioStartX, tags.audioEndX
        mtags = tags.tagID3v2 || tags.tagID3v1
        @tags = ID3::Tag2.new
        mtags ||= ID3::Tag2.new
        mtags.each{|t,v|
          begin
            if v.class == ID3::Frame
              @tags[t] = v
            else
              @tags[t] = "\3#{v}"
            end
          rescue
          end
        }
        @tags["ORIGFILENAME"] = "\3#{@filename}"
        self
      end

      def putOnDevice(mount_point)
        @dvid = NetworkWalkman.get_dvid(mount_point)
        @slot = AudioFile.freeSlot(mount_point)
        @filename = AudioFile.filename_for_slot(@slot, mount_point)
        save
      end

#       def scramble(outfile)
#         key = 0xFFFFFFFF & (( 0x2465 + @slot * 0x5296E435 ) ^ @dvid);
#         @file.seek(@audioStartX)
#         left = @audioEndX - @audioStartX
#         while left > 0 and block = @file.read([8, left].min)
#           if block.length != 8
#             outfile << block
#           else
#             outfile << block.unpack("N2").map{|x|x^key}.pack("N2")
#           end
#           left -= block.length
#         end
#       end


    end # class Mp3
  end
end
